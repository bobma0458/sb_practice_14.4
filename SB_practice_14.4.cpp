﻿#include <iostream>
#include <locale.h>

int main() 
{
    setlocale(LC_ALL, "Russian");
    std::string Practice = "Skillbox";

    std::cout << Practice << "\n";
    std::cout << "Длина строки:" << " " << Practice.length() << "\n";
    std::cout << "Первый символ:" << " " << Practice[0] << "\n";
    std::cout << "Последний символ:" << " " << Practice.back() << "\n";

    return 0;
}